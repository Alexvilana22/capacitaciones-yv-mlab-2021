# Menu

## ¿Cómo funciona GitLab? GitLab es una aplicación basada en la web con una interfaz gráfica de usuario que también puede instalarse en un servidor propio. El núcleo de GitLab lo forman los proyectos en los que se guarda el código que va a editarse en archivos digitales, los denominados repositorios.

# Crear repositorio
![alex-local](https://gitlab.com/Alexvilana22/capacitaciones-yv-mlab-2021/-/issues/9/designs/repositorio.jpg)

# Crear Grupos
![alex-local](https://gitlab.com/Alexvilana22/capacitaciones-yv-mlab-2021/-/issues/9/designs/grupo.jpg)

# Crear subgrupos
![alex-local](https://gitlab.com/Alexvilana22/capacitaciones-yv-mlab-2021/-/issues/9/designs/sub.jpg)
![alex-local](https://gitlab.com/Alexvilana22/capacitaciones-yv-mlab-2021/-/issues/9/designs/privado.jpg)

# Crear issues
![alex-local](https://gitlab.com/Alexvilana22/capacitaciones-yv-mlab-2021/-/issues/9/designs/issues.jpg)
# Crear labels
![alex-local](https://gitlab.com/Alexvilana22/capacitaciones-yv-mlab-2021/-/issues/9/designs/labels.jpg)

# Permisos

## * Los usuarios invitados pueden realizar esta acción en proyectos públicos e internos, pero no en proyectos privados. Esto no se aplica a los usuarios externos a los que se debe otorgar acceso explícito incluso si el proyecto es interno.
## * Los usuarios invitados solo pueden ver los problemas confidenciales que crearon ellos mismos.
## Si las canalizaciones públicas están habilitadas en Configuración del proyecto> CI / CD.
## * No permitido para invitados, reporteros, desarrolladores, mantenedores ni propietarios. Ver ramas protegidas.
## * Si la rama está protegida, esto depende del acceso que se les dé a los desarrolladores y mantenedores.
## * Los usuarios invitados pueden acceder a las versiones de GitLab para descargar activos, pero no pueden descargar el código fuente ni ver información del repositorio como etiquetas y confirmaciones.
## * Las acciones se limitan solo a los registros propiedad (referenciados) del usuario.
## Cuando se habilita Compartir bloqueo de grupo, el proyecto no se puede compartir con otros grupos. No afecta al grupo con el intercambio de grupos.
## * Para obtener información sobre aprobadores elegibles para solicitudes de combinación, consulte Aprobadores elegibles.
## * El permiso de propietario solo está disponible a nivel de espacio de nombres de grupo o personal (y, por ejemplo, administradores) y es heredado por sus proyectos.
## * Se aplica solo a los comentarios sobre diseños de Gestión de diseño.
## * Los usuarios solo pueden ver eventos basados ​​en sus acciones individuales.
## * En proyectos públicos e internos, la función de invitado no se aplica. Todos los usuarios pueden:

* Crea problemas.
* Dejar comentarios.
* Clonar o descargar el código del proyecto

# Agregar Miembros
![alex-local](https://gitlab.com/Alexvilana22/capacitaciones-yv-mlab-2021/-/issues/9/designs/miembros.jpg)

# Crear boards y manejo de boards
![alex-local](https://gitlab.com/Alexvilana22/capacitaciones-yv-mlab-2021/-/issues/9/designs/boards.jpg)

# Commits desde Gitlab y ramas desde Gitlab
![alex-local](https://gitlab.com/Alexvilana22/capacitaciones-yv-mlab-2021/-/issues/9/designs/commit.jpg)
![alex-local](https://gitlab.com/Alexvilana22/capacitaciones-yv-mlab-2021/-/issues/9/designs/ramas.jpg)





